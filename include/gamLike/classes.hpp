////////////////////////////////////////////////////////////
// GamLike internal class declaration
//
// Author: Christoph Weniger <c.weniger@uva.nl>
// Created: 2015-09-16
////////////////////////////////////////////////////////////

#ifndef __gamLike_classes_hpp__
#define __gamLike_classes_hpp__

#include <string>
#include <vector>
#include <gsl/gsl_integration.h>
#include "gamLike.hpp"

namespace gamLike
{
    typedef std::vector<std::pair<std::vector<double>, std::vector<double> > > LnL_vs_flux;
    typedef std::vector<std::pair<double, double> > E_bins;

    enum jMode
    {
        MARGIN,
        PROFILE,
        PROFILE_DIV_J,
        NONE,
        EXTERNAL
    };

    std::vector<double> trunc_vec(const std::vector<double> & vec, double val);


    //////////////////////////////////////////////////
    // Marginalized/profiled likelihood for one target
    //////////////////////////////////////////////////

    class GamLike: public gsl_function
    {
        public:
            GamLike(std::string filename, double J_mean, double J_err, jMode method, int filetype);
            double lnL(std::vector<double> Phi);
            double getJvalue();
            E_bins getEbins();
            // Define weigthed ROI, such that, with psi being the
            // Galacto-centric distance,
            //   int dOmega E(psi) ~ sum_i w_i E(psi_i)
            // The calculated J-value is devided by the ROI size Omega
            // afterwards (only necessary if tables contain intensities and not
            // fluxes)
            void set_weighted_ROI(Halos tag, std::string filename, double Omega = 1);

        private:
            void set_weighted_ROI(Halos tag, const std::vector<double> & psi, const std::vector<double> & weight);
            static double invoke(double J, void* par);
            double lnLprod(double J);
            int loadData_lnLtab(std::string filename);
            int loadData_Sigma(std::string filename);
            double lnL(std::vector<double> Phi, double J);
            double lnL_lnLtab(std::vector<double> Phi, double J);
            double lnL_Sigma(std::vector<double> Phi, double J);
            jMode method;  // 0: mean value; 1: marginalization; 2: profiling; 3: profiling with 1/J factor
            int filetype;  // 0: tabulated lnL; 1: flux with covariance matrix
            std::pair<std::vector<double>, std::vector<double> > weighted_ROI;
            Halos halo_tag;

            double J_mean;  // log10(GeV^2 cm^-5 sr)
            double J_err;   // log10(GeV^2 cm^-5 sr)
            gsl_integration_workspace * gsl_workspace;  // GSL workspace
            std::vector<double> my_Phi, flux;
            std::vector<std::vector<double> > Sigma;
            std::string filename;  // filename
            E_bins e_bins;  // Energy bins [GeV]
            LnL_vs_flux lnL_vs_flux;  // list of lnL as function of flux in each bin [GeV/cm^2/s]
    };


    ////////////////////////////////////////////////
    // Marginalized likelihood for multiple targets
    ////////////////////////////////////////////////

    class GamCombLike
    {
        public:
            GamCombLike() {};
            void add_target(GamLike&);
            void add_target(std::string filename, double J_mean, double J_err, jMode mode, int filetype);
            double lnL(std::vector<double> Phi);
            double lnL(std::vector<double> E, std::vector<double> dPhidE);
            E_bins getEbins();
//            void set_weighted_ROI(Halos tag, const std::vector<double> & psi,
//                    const std::vector<double> & weight);

        private:
            std::vector<GamLike> my_targets;
            E_bins e_bins;
    };


    ////////////////////
    // Helper functions 
    ////////////////////

    // Normal distribution with mean x0 and standard deviation dx
    double normal_pdf(double x0, double dx, double x);

    // Interpolation in log-log space
    double interpolate(double x, const std::vector<double> & xlist, const std::vector<double> & ylist, bool zerobound);

    class IntSpec: public gsl_function
    {
        public:
            IntSpec(double* Energy, double* dNdE);
            IntSpec(double* Energy, double* dNdE, int arraySize);
            IntSpec(std::vector<double> Energy, std::vector<double> dNdE);
            static double invoke(double x, void* params);
            double integrate(double x0, double x1);

        private:
            std::vector<double> Energy;
            std::vector<double> dNdE;
            gsl_integration_workspace * gsl_workspace;  // GSL workspace
    };
}

#endif /* defined(__gamLike_classes_hpp__) */
