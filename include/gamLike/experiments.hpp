////////////////////////////////////////////////////////////
// GamLike list of experiments
//
// Author: Christoph Weniger <c.weniger@uva.nl>
// Created: 2015-09-16
////////////////////////////////////////////////////////////


#ifndef __gamLike_experiments_hpp__
#define __gamLike_experiments_hpp__

#include "gamLike.hpp"

namespace gamLike
{
    // Global pointers and variables
    void set_experiments_path(const std::string & path);
    std::vector<Experiments> get_used_experiments();

    // Likelihood definitions
    void init_fermi_dwarfs_pass7();
    double lnL_fermi_dwarfs_pass7(const std::vector<double> & E, const std::vector<double> & dPhidE);

    void init_fermi_dwarfs_pass8();
    double lnL_fermi_dwarfs_pass8(const std::vector<double> & E, const std::vector<double> & dPhidE);

    void init_fermi_gc_CaloreA();
    double lnL_fermi_gc_CaloreA(const std::vector<double> & E, const std::vector<double> & dPhidE);

    void init_fermi_gc_Calore_ExtJ();
    double lnL_fermi_gc_Calore_ExtJ(const std::vector<double> & E, const std::vector<double> & dPhidE);

    void init_fermi_gc_CaloreB();
    double lnL_fermi_gc_CaloreB(const std::vector<double> & E, const std::vector<double> & dPhidE);

    void init_fermi_gc_Calore_HEP();
    double lnL_fermi_gc_Calore_HEP(const std::vector<double> & E, const std::vector<double> & dPhidE);

    void init_CTA_Silverwood();
    double lnL_CTA_Silverwood(const std::vector<double> & E, const std::vector<double> & dPhidE);

    void init_HESS_Abramowski_integrated();
    double lnL_HESS_Abramowski_integrated(const std::vector<double> & E, const std::vector<double> & dPhidE);

    void init_HESS_Abramowski_binned();
    double lnL_HESS_Abramowski_binned(const std::vector<double> & E, const std::vector<double> & dPhidE);

    void init_HESS_Abramowski_integrated_ExtJ();
    double lnL_HESS_Abramowski_integrated_ExtJ(const std::vector<double> & E, const std::vector<double> & dPhidE);

    void init_HESS_Abramowski_binned_ExtJ();
    double lnL_HESS_Abramowski_binned_ExtJ(const std::vector<double> & E, const std::vector<double> & dPhidE);

}

#endif /* defined(__gamLike_experiments_hpp__) */
