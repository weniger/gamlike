////////////////////////////////////////////////////////////
// GamLike Line-of-Sight integration
//
// Author: Christoph Weniger <c.weniger@uva.nl>
// Created: 2016
////////////////////////////////////////////////////////////

void los_integral_cpp(
        std::vector<double> r, std::vector<double> f, double D,  // Input
        std::vector<double> & phi, std::vector<double> & los // Output
        );
