////////////////////////////////////////////////////////////
// GamLike C++ API 
//
// Author: Christoph Weniger <c.weniger@uva.nl>
// Created: 2015-09-16
////////////////////////////////////////////////////////////

#ifndef __gamLike_gamLike_hpp__
#define __gamLike_gamLike_hpp__

#include <vector>
#include <string>
#include <map>

namespace gamLike
{
    // List of available experiments
    enum Experiments
    {
        FermiLAT_dSphs_2013,               //  0
        FermiLAT_dSphs_2015,               //  1
        FermiLAT_GC_CaloreA_2014,          //  2
        FermiLAT_GC_CaloreB_2014,          //  3
        FermiLAT_GC_Achterberg_2015,       //  4
        CTA_Silverwood,                    //  5
        HESS_Abramowski_integrated,        //  6
        HESS_Abramowski_binned,            //  7
        FermiLAT_GC_Calore_2014_ExtJ,      //  8
        HESS_Abramowski_integrated_ExtJ,   //  9
        HESS_Abramowski_binned_ExtJ,       // 10
    };

    // List of internal modifiable DM profiles
    enum Halos
    {
        MilkyWay                      // 0
    };

    // Interface functions
    //   E: Energy [GeV]
    //   dPhidE:  dNdE * sv / m**2 / 8 pi [cm3/s/GeV3]
    extern "C" void set_data_path(const std::string & path);
    extern "C" void init(Experiments tag);
    extern "C" double lnL(Experiments tag, const std::vector<double> & E, const std::vector<double> & dPhidE);
    extern "C" std::map<Experiments, double> lnL_map(const std::vector<double> & E, const std::vector<double> & dPhidE);
    extern "C" void set_halo_profile(Halos, const std::vector<double> & r, const std::vector<double> & rho, double dist);

    // Global structures to store halo and emission profiles
    extern std::map<Halos, std::pair<std::vector<double>,std::vector<double> > > profiles;
    extern std::map<Halos, std::pair<std::vector<double>,std::vector<double> > > emission;
}

#endif /* defined(__gamLike_gamLike_hpp__) */
