////////////////////////////////////////////////////////////
// GamLike C-API 
//
// Author: Christoph Weniger <c.weniger@uva.nl>
// Created: 2015-09-16
////////////////////////////////////////////////////////////

#ifndef __gamLike_gamLike_C_h__
#define __gamLike_gamLike_C_h__

namespace gamLike
{
    // C API
    extern "C" void set_data_path_(char const* const path);
    extern "C" void init_(Experiments &tag);
    extern "C" double lnl_(Experiments &tag, double const* const E, double const* const dPhidE, int &size);
    extern "C" void set_halo_profile_(Halos &tag, double const* const r, double const* const rho, int & size, double & dist);
}

#endif /* defined(__gamLike_gamLike_C_h__) */
