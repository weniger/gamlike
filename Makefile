# Set directories
BUILD=./build
SRC=./src
LIB=./lib
INC=./include

# Standard tools
CXX=g++
AR=ar

# Flags
# Note: GAMLIKE_DATA_PATH=/path/to/gamLike/data/ (trailing "/" matters) sets
# default data directory.
CXXFLAGS=-fPIC
ifdef GAMLIKE_DATA_PATH
CPPFLAGS=-D GAMLIKE_DATA_PATH=\"$(GAMLIKE_DATA_PATH)\"
endif
ARFLAGS=rv
LDFLAGS=-shared -rdynamic
LDLIBS=-lgsl -lgslcblas

# Source files
SOURCES=experiments.cpp classes.cpp los_integral.cpp gamLike.cpp gamLike_C.cpp

# Object files derived from sources
OBJ = $(patsubst %.cpp,$(BUILD)/%.o,$(SOURCES)) 

all: $(LIB)/gamLike.a $(LIB)/gamLike.so

$(BUILD)/%.o: $(SRC)/%.cpp $(INC)/gamLike.hpp $(INC)/gamLike/experiments.hpp $(INC)/gamLike/los_integral.hpp $(INC)/gamLike/classes.hpp $(INC)/gamLike_C.h
	$(CXX) -I $(INC) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@

$(LIB)/gamLike.a: $(OBJ)
	$(AR) $(ARFLAGS) $@ $^

$(LIB)/gamLike.so: $(OBJ)
	$(CXX) $(CXXFLAGS) -o $@ $(LDFLAGS) $^ $(LDLIBS)

.PHONY: clean tarball
clean:
	rm -f $(BUILD)/*.o
	rm -f $(LIB)/*.o
	rm -f $(BUILD)/*.so
	rm -f $(LIB)/*.so
	rm -f $(BUILD)/*.a
	rm -f $(LIB)/*.a

tarball:
	git archive --prefix=gamLike-${VERSION}/ -o gamLike-${VERSION}.tar.gz v${VERSION}

$(shell mkdir -p $(BUILD))
$(shell mkdir -p $(LIB))
