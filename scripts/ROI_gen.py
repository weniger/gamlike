#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Generation of ROI files, required for handling externally provided DM profiles
#
# Author: Christoph Weniger <c.weniger@uva.nl>
# Date: Mar 2017

from __future__ import division
from numpy import *
import healpy
import pylab as plt

def generate_ROI_file(ROI, theta_min = None, theta_max = None, theta_steps =
        None, phi_steps = None):

    def getRing(theta, N):
        theta = radians(theta)*ones(N)
        phi = 2*pi*arange(N)/N

        vec = healpy.ang2vec(theta, phi)
        vec[:,0], vec[:,2] = vec[:,2].copy(), vec[:,0].copy()
        theta, phi = healpy.vec2ang(vec)
        b = degrees(pi/2-theta)
        l = degrees(phi)
        l = mod(l+180,360)-180
        plt.plot(l, b)
        return l, b

    theta_list = logspace(log10(theta_min), log10(theta_max), theta_steps)
    theta_factor = theta_list[1]/theta_list[0]

    outlist = []

    for i, theta in enumerate(theta_list):
        theta_width = theta*(sqrt(theta_factor)-1/sqrt(theta_factor))  # ring width

        l, b = getRing(theta, phi_steps)
        cont = ROI(l, b).sum()/phi_steps*radians(theta_width)*sin(radians(theta))*2*pi
        if cont > 0:
            outlist.append([theta, cont])

    outlist = array(outlist)
    Omega = sum(outlist.T[1])
    print "Total region size [sr]:", Omega
    return outlist

def generate(mode = ""):
    if mode == "Calore2014":
        ROI = lambda l, b: (abs(l)<=20) & (abs(b)>2) & (abs(b)<=20)
        l = generate_ROI_file(ROI, theta_min = 0.001, theta_max = 30, theta_steps =
                1000, phi_steps = 1000)
        savetxt("../data/Calore+_2014/ROI.txt", l)
    elif mode == "Abramowski2011":
        # Ignoring the "hole" in the ON-region
        ROI = lambda l, b: (sqrt(l**2+b**2) < 1) & (abs(b) > 0.3)
        l_ON = generate_ROI_file(ROI, theta_min = 0.1, theta_max = 3, theta_steps =
                1000, phi_steps = 1000)
        #savetxt("../data/Abramowski+_2011/ROI_ON.txt", l_ON)
        ROI = lambda l, b: (sqrt(l**2+b**2) > 1.3) & (sqrt(l**2+b**2) < 1.55) & (abs(b) > 0.3)
        # Approximate OFF region as thin ring
        l_OFF = generate_ROI_file(ROI, theta_min = 0.1, theta_max = 3, theta_steps =
                1000, phi_steps = 1000)
        #savetxt("../data/Abramowski+_2011/ROI_OFF.txt", l_OFF)
        l_OFF[:,1] *= -1
        l_ON_minus_OFF = vstack((l_ON, l_OFF))
        savetxt("../data/Abramowski+_2011/ROI_ON_minus_OFF.txt", l_ON_minus_OFF)
    elif mode == "Silverwood2014":
        ROI = lambda l, b: (sqrt(l**2+(b-1.42)**2) > 0.55) & (sqrt(l**2+(b-1.42)**2) < 2.88) & (b < 4) & (b > 3) & (l>1)
        l = generate_ROI_file(ROI, theta_min = 0.1, theta_max = 5, theta_steps =
                1000, phi_steps = 1000)
        savetxt("../data/Silverwood+_2014/ROI_Seg1.txt", l)

if __name__ == "__main__":
    generate(mode = "Calore2014")
    #generate(mode = "Abramowski2011")
    #generate(mode = "Silverwood2014")
