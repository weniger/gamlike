#!/usr/bin/env python

from __future__ import division
from numpy import *
from ctypes import *
import DSspectra
import pylab as plt
import iminuit

DATA_PATH = '../data/'

def _dNdE_Tasitsiomi(E, mass):
    x = E/mass
    f = lambda x: 5/6*x**1.5 - 10/3*x + 5*x**0.5 + 5/6*x**-0.5-10/3
    if x < 0.0001 or x > 1: return .0  # return .0 (not an integer)
    dx = 0.000001
    dfdx = -(f(x)-f(x-dx))/dx
    return dfdx/mass
dNdE_Tasitsiomi = vectorize(_dNdE_Tasitsiomi)

def ToughInputTests(mode = None, Emin = None, Emax = None):
    pHandle = cdll.LoadLibrary('../lib/gamLike.so')
    pHandle.set_data_path_(DATA_PATH)
    pHandle.init_(byref(c_int(mode)))
    pHandle.lnl_.restype = c_double

    nbins = 10000
    mass = 100
    sv = 1e-25

    def printComp(a, b):
        return "%.3e %.3e %.4e"%(a, b, (a-b)/(a+b)*2)

    def boundaries(Emin, Emax):
        dNdE = lambda E: E**-2.0* (sign(Emax-E)+1)*(sign(E-Emin)+1)/4
        dPhidE = lambda E: dNdE(E) / 8 / pi / mass**2 * sv
        Elist = logspace(log10(Emin*0.5), log10(Emax*2), nbins)
        lnL1 = pHandle.lnl_(byref(c_int(mode)), (c_double * nbins)(*Elist), (c_double * nbins)(*dPhidE(Elist)), byref(c_int(nbins)))
        dNdE = lambda E: E**-2.0
        dPhidE = lambda E: dNdE(E) / 8 / pi / mass**2 * sv
        Elist = logspace(log10(Emin), log10(Emax), nbins)
        lnL2 = pHandle.lnl_(byref(c_int(mode)), (c_double * nbins)(*Elist), (c_double * nbins)(*dPhidE(Elist)), byref(c_int(nbins)))
        return printComp(lnL1, lnL2)

    def peaks(E0, dE, factor = 1):
        dNdE = lambda E: 1/sqrt(2*pi*dE**2)*exp(-1/2/dE**2*(E-E0)**2) * factor*E0**-2
        dPhidE = lambda E: dNdE(E) / 8 / pi / mass**2 * sv
        Elist = logspace(log10(Emin*0.5), log10(Emax*2), nbins)
        lnL = pHandle.lnl_(byref(c_int(mode)), (c_double * nbins)(*Elist), (c_double * nbins)(*dPhidE(Elist)), byref(c_int(nbins)))
        return -lnL

    Ec = sqrt(Emin*Emax)

    print "Testing consistent treatment of boundaries (lnL should be consistent)"
    for E in [Emin, Ec, Emax]:
        print "%.1e: %s"%(E, boundaries(E*0.5, E*2))
    print

    print "Testing spectrum integration for sharp peaks (lnL should be consistent)"
    print "dE/E: 0.001, 0.01"
    for E0 in logspace(log10(Emin*1.1), log10(Emax*0.9), 5):
        print "%.2e: %s"%(E0, printComp(peaks(E0, E0*0.01), peaks(E0, E0*0.005)))
    print

    print "Testing extreme values (lnL should be not nan / crash etc)"
    factors = hstack([-logspace(200, 0, 5), [0], logspace(0, 200, 5)])
    for factor in factors:
        print "%.1e: %.4e"%(factor, peaks(Ec, Ec*10, factor = factor))


def DwarfLimits(channel = None, version = None):
    plt.clf()
    pHandle = cdll.LoadLibrary('../lib/gamLike.so')
    pHandle.set_data_path_(DATA_PATH)

    if channel == 'bb':
        MMIN, CHANNEL = 5, 'bb'
    elif channel == 'tautau':
        MMIN, CHANNEL = 1.8, 'tautau'
    elif channel == 'mumu':
        MMIN, CHANNEL = 0.11, 'mm'
    elif channel == 'WW':
        MMIN, CHANNEL = 80.4, 'WW'

    def plotUL(mode = None, style = '', label = ''):
        pHandle.init_(byref(c_int(mode)))
        pHandle.lnl_.restype = c_double

        def lnL_function(mass, sv):
            # Replace this with a function that calculates bb spectra
            dNdE = DSspectra.spec(channel=CHANNEL, mass=mass, type='gam')
            dPhidE = lambda E: dNdE(E) / 8 / pi / mass**2 * sv
            Elist = logspace(log10(0.1), log10(500), 100)
            lnL = pHandle.lnl_(byref(c_int(mode)), (c_double * 100)(*Elist), (c_double * 100)(*dPhidE(Elist)), byref(c_int(100)))
            print lnL
            return -lnL

        massList = logspace(log10(MMIN), 4, 20)
        svList = logspace(-27, -20, 20)
        svUL_list = []
        lnLArray = []
        for mass in massList:
            print "Working on mass = ", mass
            lnLList = array([lnL_function(mass, sv) for sv in svList])
            lnLArray.append(lnLList)
            i = list(lnLList).index(min(lnLList))
            f = lambda x: interp(x, lnLList[i:], svList[i:])
            svUL = f(2.71/2+min(lnLList))
            svUL_list.append(svUL)
        lnLArray = array(lnLArray)
        lnLArray -= lnLArray.min()
        plt.imshow(lnLArray.T[::-1], extent = (log10(MMIN), 4, -27, -20),
                interpolation='nearest', cmap = 'cubehelix_r', vmin=0, vmax = 10, aspect='auto')
        plt.plot(log10(massList), log10(array(svUL_list)), style, label=label)

    if version == 'pass8':
        data = loadtxt('../data/Ackermann+_2015/limits_%s.txt'%CHANNEL).T
        plt.plot(log10(data[0]), log10(data[26]), 'g:', label="their P8")
        plotUL(1, style = 'g-', label = 'my P8')
        paper = 'Ackermann+_2015'
    elif version == 'pass7':
        data = loadtxt('../data/Ackermann+_2013/limits_%s.txt'%CHANNEL).T
        plt.plot(log10(data[0]), log10(data[-1]), 'r:', label="their P7")
        plotUL(0, style = 'r-', label = 'my P7')
        paper = 'Ackermann+_2013'

    plt.colorbar()

    plt.legend(frameon=True, loc=2)
    plt.ylim([-27, -20])
    plt.xlabel("Mass [GeV]")
    plt.ylabel("Cross-section sv [cm3 s]")
    plt.savefig("comparison_%s_%s.eps"%(paper, CHANNEL))


def DwarfLimitsLikelihoodMap(channel = None, version = None):
    plt.clf()
    plt.figure(figsize=(6,5))
    pHandle = cdll.LoadLibrary('../lib/gamLike.so')
    pHandle.set_data_path_(DATA_PATH)

    mode = 1 # P8 dwarfs
    pHandle.init_(byref(c_int(mode)))
    pHandle.lnl_.restype = c_double

    def lnL_function(mass, sv):
        # Replace this with a function that calculates bb spectra
        E0 = mass
        dE = 0.01*E0
        dNdE = lambda E: 1/sqrt(2*pi)/dE*exp(-(E-E0)**2/2/dE**2) * 2  # A gamma-ray line pair
        dPhidE = lambda E: dNdE(E) / 8 / pi / mass**2 * sv
        Elist = logspace(log10(E0/2), log10(E0*2), 100)
        lnL = pHandle.lnl_(byref(c_int(mode)), (c_double * 100)(*Elist), (c_double * 100)(*dPhidE(Elist)), byref(c_int(100)))
        #print lnL
        return -lnL

    svList = logspace(-26, -29, 100)
    massList = logspace(log10(0.5), 2, 100)
    lnLarray = zeros((len(svList), len(massList)))

    for i, sv in enumerate(svList):
        for j, mass in enumerate(massList):
            lnLarray[i, j] = lnL_function(mass, sv)

    lnLarray -= lnLarray.min()

    plt.imshow(2*lnLarray, interpolation='nearest', vmin = 0, vmax = 9,
            extent=[min(log10(massList)), max(log10(massList)),
                min(log10(svList)), max(log10(svList))], aspect=0.8)
    plt.colorbar()
    plt.xlabel("log10(mass/GeV)")
    plt.ylabel("log10(sv / cm3/s)")
    plt.tight_layout()
    plt.savefig("Dwarf_likelihood_map.eps")
    
    #print lnL_function(40, 1e-26)


def CaloreGC_fits(mode = 2):
    pHandle = cdll.LoadLibrary('../lib/gamLike.so')
    pHandle.set_data_path_(DATA_PATH)
    pHandle.init_(byref(c_int(mode)))
    pHandle.lnl_.restype = c_double

    def lnL(mass, sv, channel = 'bb'):
        dNdE = DSspectra.spec(channel=channel, mass=mass, type='gam')
        dPhiJdE = lambda x: dNdE(x) * sv/mass/mass/8/pi
        Elist = logspace(log10(0.3), log10(100), 100)

        lnL = pHandle.lnl_(byref(c_int(mode)), (c_double * 100)(*Elist), (c_double * 100)(*dPhiJdE(Elist)), byref(c_int(100)))

        return -lnL

    for channel in ['gg', 'tautau', 'bb']:
        if channel != 'bb': continue
        m = iminuit.Minuit(lambda mass, sv: lnL(mass, sv*1e-26, channel =
            channel), errordef = 0.5, mass = 40, sv = 1, error_mass = .1, error_sv = 1)
        m.migrad()
        m.minos(var='mass', sigma=1.0)

def CaloreGC_lnLmap(mode = 2):
    pHandle = cdll.LoadLibrary('../lib/gamLike.so')
    pHandle.set_data_path_(DATA_PATH)
    pHandle.init_(byref(c_int(mode)))
    pHandle.lnl_.restype = c_double

    if mode == 8:
        # Set halo profile to standard NFW with 0.4 local density and rs = 20 kpc
        # scale radius.
        rlist = logspace(-2, 2.5, 100)
        rs = 20
        gamma = 1.26
        rholist = 1./rlist**gamma/(rlist/rs+1)**(3-gamma)
        rholist *= 0.4/interp(8.5, rlist, rholist)
        pHandle.set_halo_profile_(byref(c_int(0)), (c_double * 100)(*rlist), (c_double * 100)(*rholist), byref(c_int(100)), byref(c_double(8.5)))
#
    def _lnL(mass, sv, channel = 'bb'):
        dNdE = DSspectra.spec(channel=channel, mass=mass, type='gam')
        dPhiJdE = lambda x: dNdE(x) * sv/mass/mass/8/pi
        Elist = logspace(log10(0.3), log10(100), 100)
        lnL = pHandle.lnl_(byref(c_int(mode)), (c_double * 100)(*Elist), (c_double * 100)(*dPhiJdE(Elist)), byref(c_int(100)))
        return -lnL

    lnL = vectorize(_lnL)
    mmin, mmax = 10, 200
    svgrid, massgrid = meshgrid(logspace(-24, -28, 30), logspace(log10(mmin), log10(mmax), 30))
    data = lnL(massgrid, svgrid).T
    print data.min()
    data -= data.min()
    plt.imshow(data, extent = [log10(mmin), log10(mmax), -28, -24], aspect = 'auto', cmap='cubehelix_r', vmin=0, vmax=10, interpolation='nearest')
    plt.colorbar()
    plt.xlabel('log10(mass/GeV)')
    plt.ylabel('log10(sv/cm3s-1)')
    plt.plot(log10(48.22), log10(1.742e-26), 'o')
    plt.show()

def CTAlimits():
    pHandle = cdll.LoadLibrary('../lib/gamLike.so')
    pHandle.set_data_path_(DATA_PATH)

    def plotUL(mode = None, style = '', label = ''):
        pHandle.init_(byref(c_int(mode)))
        pHandle.lnl_.restype = c_double

        def lnL_function(mass, sv):
            # Replace this with a function that calculates bb spectra
            dNdE = DSspectra.spec(channel='bb', mass=mass, type='gam')
            dPhidE = lambda E: dNdE(E) / 8 / pi / mass**2 * sv
            Elist = logspace(log10(10), log10(10000), 1000)
            lnL = pHandle.lnl_(byref(c_int(mode)), (c_double * 1000)(*Elist), (c_double * 1000)(*dPhidE(Elist)), byref(c_int(1000)))
            print lnL
            return -lnL

        massList = logspace(log10(50), 4, 20)
        svList = logspace(-28, -23, 40)
        svUL_list = []
        for mass in massList:
            print "Working on mass = ", mass
            lnLList = []
            for sv in svList:
                lnL = lnL_function(mass, sv)
                lnLList.append(lnL)
            i = lnLList.index(min(lnLList))
            lnLList = array(lnLList)
            f = lambda x: interp(x, lnLList[i:], svList[i:])
            svUL = f(2.71/2+min(lnLList))
            #svUL = f(1.64485/2+min(lnLList))
            svUL_list.append(svUL)
        plt.loglog(massList, array(svUL_list), style, label=label)

    plotUL(5, style = 'r-', label = 'CTA Silverwood')

    m, sv = 10**loadtxt('../data/Silverwood+_2014/bb_limits.dat').T
    plt.loglog(m, sv, label='original 100h 1%')

    plt.axhline(3e-26)
    plt.legend(frameon=False, loc=2)
    plt.ylim([1e-27, 1e-23])
    plt.xlabel("Mass [GeV]")
    plt.ylabel("Cross-section sv [cm3 s]")
    plt.savefig("comparison_Silverwood+_2014.eps")

def HESSlimits(ExtJ = False):
    pHandle = cdll.LoadLibrary('../lib/gamLike.so')
    pHandle.set_data_path_(DATA_PATH)

    if ExtJ:
        # Set halo profile to standard NFW with 0.4 local density and rs = 20 kpc
        # scale radius.
        rlist = logspace(-2, 2.5, 100)
        rs = 20
        gamma = 1.00
        rholist = 1./(rlist+.000001)**gamma/(rlist/rs+1)**(3-gamma)
        rholist *= 0.40/interp(8.5, rlist, rholist)
        pHandle.set_halo_profile_(byref(c_int(0)), (c_double * 100)(*rlist), (c_double * 100)(*rholist), byref(c_int(100)), byref(c_double(8.5)))

    def plotUL(mode = None, style = '', label = ''):
        pHandle.init_(byref(c_int(mode)))
        pHandle.lnl_.restype = c_double

        def lnL_function(mass, sv):
            # Replace this with a function that calculates bb spectra
            dNdE = lambda e: dNdE_Tasitsiomi(e, mass)  # Used in HESS paper
            #dNdE = DSspectra.spec(channel='bb', mass=mass, type='gam')
            dPhidE = lambda E: dNdE(E) / 8 / pi / mass**2 * sv
            Elist = logspace(log10(1), log10(100000), 500)
            lnL = pHandle.lnl_(byref(c_int(mode)), (c_double * 500)(*Elist), (c_double * 500)(*dPhidE(Elist)), byref(c_int(500)))
            print lnL
            return -lnL

        massList = logspace(log10(100), 7, 50)
        svList = logspace(-27, -21, 50)
        svUL_list = []
        for mass in massList:
            print "Working on mass = ", mass
            lnLList = []
            for sv in svList:
                lnL = lnL_function(mass, sv)
                lnLList.append(lnL)
            i = lnLList.index(min(lnLList))
            lnLList = array(lnLList)
            f = lambda x: interp(x, lnLList[i:], svList[i:])
            svUL = f(2.71/2+min(lnLList))
            svUL_list.append(svUL)
        plt.loglog(massList, array(svUL_list), style, label=label)

    plotUL(6, style = 'r-', label = 'HESS GC (paper - one bin)')
    plotUL(7, style = 'g-', label = 'HESS GC (paper - binned)')

    if ExtJ:
        plotUL(9, style = 'r:', label = 'HESS GC ExtJ (paper - one bin)')
        plotUL(10, style = 'g:', label = 'HESS GC ExtJ (paper - binned)')

    x,y = loadtxt(DATA_PATH+"Abramowski+_2011/Abramowski_2011_HESS_GC_qqbar.txt").T
    plt.loglog(x, y*5/3, 'k--', label="Orig. paper result (NFW)") # plot for NFW profile instead (table shows Einasto results)
    plt.loglog(x, y, 'k:', label="Orig. paper result (Einasto)")

    plt.legend(frameon=True, loc=4)
    plt.ylim([1e-26, 1e-21])
    plt.xlim([100, 10000000])
    plt.xlabel("Mass [GeV]")
    plt.ylabel("Cross-section sv [cm3 s]")
    plt.savefig("comparison_Abramowski+_2011.eps")

if __name__ == '__main__':
    pass
    # Comment out whatever test you'd like you run

    #DwarfLimits(channel = 'bb', version = 'pass8')
    #DwarfLimits(channel = 'tautau', version = 'pass8')
    #DwarfLimits(channel = 'mumu', version = 'pass8')
    #DwarfLimits(channel = 'WW', version = 'pass8')
    #ToughInputTests(mode = 0, Emin = 0.5, Emax = 500) # Pass7 dwarfs

    #DwarfLimits(channel = 'bb', version = 'pass7')
    #ToughInputTests(mode = 1, Emin = 0.5, Emax = 500) # Pass8 dwarfs

    #HESSlimits(ExtJ=True)
    #ToughInputTests(mode = 6, Emin = 260, Emax = 30000) # Abramowski one bin
    #ToughInputTests(mode = 7, Emin = 260, Emax = 30000) # Abramowski binned

    #CaloreGC_fits(mode = 2)
    #CaloreGC_fits(mode = 3)
    #CaloreGC_fits(mode = 4)
    #CaloreGC_lnLmap(mode = 2)
    #CaloreGC_lnLmap(mode = 3)
    #CaloreGC_lnLmap(mode = 4)
    #CaloreGC_lnLmap(mode = 8)
    #ToughInputTests(mode = 2, Emin = 0.3, Emax = 500) # GC CaloreA 2014 p
    #ToughInputTests(mode = 3, Emin = 0.3, Emax = 500) # GC CaloreB 2014 p
    #ToughInputTests(mode = 4, Emin = 0.3, Emax = 500) # GC Achterberg 2015 p

    #CTAlimits()
    #ToughInputTests(mode = 5, Emin =  25, Emax = 10000) # Silverwood 2014

    #DwarfLimitsLikelihoodMap()
