gamLike

Likelihoods for indirect dark matter searches with gamma rays

  Author: Christoph Weniger <c.weniger@uva.nl>
  Created: Jul 2014
  Major updates: Jan 2016, Sep 2016
  Version: 1.0.0 (May 2017)


Installation
============

gamLike has been tested with GNU C and C++ compilers (5.2.1), and requires gsl
libraries for some of the integration routines.  Adjust the flags in the
Makefile, run

  make

and the static and shared gamLike libraries are generated in the ./lib folder.


Usage
=====

The relevant interface is defined in include/gamLike.hpp (with another Fortran
friendly version in include/gamLike_C.hpp).  Tests and examples in Python can
be found in scripts/unit_tests.py.


List of implemented likelihoods
===============================

- FermiLAT_dSph_2013: Combined Fermi pass7 dSph limits (2013)
  Ackermann, M. et al. 2014, Phys. Rev. D, 89, 042001 (arXiv:1310.0828)
  (15 dwarfs, profiling over J-value uncertainties)

- FermiLAT_dSph_2015: Combined Fermi pass8 dSph limits (2015)
  Ackermann, M. et al. 2015, Phys. Rev. Lett. 115 (2015) 23, 231301 (arXiv:1503.02641)
  (15 dwarfs, profiling over J-value uncertainties)

- Fermi GeV excess likelihood
  based on Calore, Cholis and CW, JCAP 1503 (2015) 038 (arXiv:1409.0042)
  (including estimate for covariance of foreground systematics)
  - FermiLAT_GC_CaloreA_2014: without J-value uncertainties (as in JCAP 1503 (2015) 038)
  - FermiLAT_GC_CaloreB_2014: marginalized J-value uncertainites (as in Phys.Rev. D91 (2015) 6, 063003)
  - FermiLAT_GC_Achterberg_2015: marginalized J-value uncertainties & 10% HEP systematics (as in JCAP 1508 (2015) 08, 006)
  - FermiLAT_GC_CaloreExt_2014: with J-value calculated for input profile.

- CTA projections
  based on Silverwood et al., JCAP 1503 (2015) 03, 055 (arXiv:1408.4131)
  (including 1% instr. systematics, for 100h observations, no J-value
  uncertainties)

- HESS-I Halo analysis
  based on Abramowski+ 2011, Phys. Rev. Lett. 106 (2011) 161301 (arXiv:1103.3266)
  - HESS_Abramowski_integrated: Reproduces PRL results (just one energy bin)
  - HESS_Abramowski_binned: Takes into account energy bins as shown in PRL
    (is more accurate for spectra with features)


Notes:
- The dark matter profiles adopted for the individual likelihoods are the ones
  that were used in the original publications.
- The Milky Way profile can be set externally for some of the likelihoods.
- The effects of energy dispersion are currently not implemented for any of the
  experiments.  This is approximately OK for spectra like bb or mu+ mu-, but
  miserably fails for gamma-ray lines or virtual internal Bremsstrahlung.


How to cite
===========

When using this package, please cite the relevant original experimental papers
as well as the GAMBIT DarkBit paper (arXiv:1705.xxxxx), which is the release
paper for this package.