#!/usr/bin/env python

# Process fluxes
# Author: Christoph Weniger 
# Date: 8 Oct 2015

from __future__ import division
from numpy import *
import pylab as plt

data = loadtxt("HESS_Abramowski2011_raw.dat").T
dataF = loadtxt("HESS_Abramowski2011_fluxes_raw.dat").T
#emeans_fig = 10**data[0]*1000  # GeV
nbins = len(data[0])

edges = logspace(log10(265), log10(33500), nbins+1)
ebins = array(zip(edges[:-1], edges[1:]))
emeans = sqrt(ebins[:,0]*ebins[:,1])
de = ebins[:,1] - ebins[:,0]
#print emeans/emeans_fig

flux_sig = dataF[1]/(emeans/1e3)**2.7  # 1/TeV/sr/m2/s
flux_bkg  = dataF[2]/(emeans/1e3)**2.7  # 1/TeV/sr/m2/s
flux_sig /= 1e4*1e3/de  # 1/sr/cm2/s
flux_bkg /= 1e4*1e3/de  # 1/sr/cm2/s

#plt.loglog(emeans, flux_sig*emeans**2.7/de)
#plt.loglog(emeans, flux_bkg*emeans**2.7/de)
#plt.show()

res = data[1]  # (flux_sig - flux_bkg)/flux_diff_err
flux_diff_err = 1/res*(flux_sig - flux_bkg)

cut = array([True, False, True, False, False, False, False, False, False, False,
        True, False, False, False, False, False, False, False, False, False,
        True, False, False, True, False, False, False, False, False, False,
        False, False, False, False, True])
#plt.loglog(emeans[cut], flux_diff_err[cut])
#cut += 1
#plt.loglog(emeans[cut], flux_diff_err[cut])

flux_diff_err_cleaned = interp(emeans, emeans[cut], flux_diff_err[cut])
flux_res = res * flux_diff_err_cleaned

# This here is more accurate (takes into account spectral information)
out = []
for i, ebin in enumerate(ebins):
    for intensity in logspace(-15, -5, 40):
        # MeV | MeV | MeV/cm2/s/sr | lnL
        lnL = -0.5*(intensity-flux_res[i])**2/flux_diff_err_cleaned[i]**2 + 0.5*(flux_res[i])**2/flux_diff_err_cleaned[i]**2
        out.append([ebin[0]*1000, ebin[1]*1000, intensity*emeans[i]*1000, lnL])
savetxt('HESS_Abramowski2011_binned.dat', out)


# This here reproduces the results from Abramowski+2011
out = []
    # The energy range is slightly extended w.r.t. what is shown in the paper
    # Fig. 3, which allows to reproduce the shape of the exclusion plot at all
    # DM masses as shown in Fig. 4 (using the correct analytical qqbar
    # spectrum).
for ebin in [[230, 30000]]:  
    for intensity in logspace(-15, -5, 40):
        # MeV | MeV | MeV/cm2/s/sr | lnL
        flux_diff_err_all = sqrt((flux_diff_err_cleaned**2).sum())  # Combined error
        lnL = -0.5*(intensity)**2/flux_diff_err_all**2
        emean = sqrt(prod(ebin))
        out.append([ebin[0]*1000, ebin[1]*1000, intensity*emean*1000, lnL])
savetxt('HESS_Abramowski2011_integrated.dat', out)

