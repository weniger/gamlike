#!/usr/bin/env python
from __future__ import division
from numpy import *
import pylab as plt
from scipy import linalg

# Transform original covariance matrix from Calore+ 2014 paper into gamLike
# format; add HEP uncertainties
# Author: Christoph Weniger <c.weniger@uva.nl>
# Date: Oct 2015

def save(filename, withHep = False):
    # Read covariance matrix from Calore+ 2014
    data = loadtxt('covariance.dat')

    # Define new covariance matrix
    out = zeros((24, 27))

    # Take fluxes and energy ranges
    out[:,0] = data[:,0]
    out[:,1] = data[:,1]
    out[:,2] = data[:,2]
    sigma = data[:,-24:]

    # Invert covariance matrix
    if withHep:
        for i in range(len(sigma)):
            sigma[i, i] += (out[i,2]*0.1)**2
    out[:,3:] = linalg.inv(sigma)

    # Transform to flux per energy bin
    emeans = (out[:,0] * out[:,1])**0.5
    de = (out[:,1] - out[:,0])
    out[:,2] *= emeans**-2 * de
    a, b = meshgrid(emeans**-2 * de, emeans**-2 * de)
    out[:,3:] /= a*b

    # And write
    savetxt(filename, out)

save('like_GC_Calore.txt', False)
save('like_GC_Calore_withHEPunc.txt', True)
