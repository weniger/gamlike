////////////////////////////////////////////////////////////
// GamLike LOS integrals
//
// Author: Christoph Weniger <c.weniger@uva.nl>
// Created: Jun 2016
////////////////////////////////////////////////////////////

#include <cmath>
#include <iostream>
#include <vector>
#include <stdexcept>
#include <gsl/gsl_sf_hyperg.h>

/* Returns numerically reasonably stable results for 2F1(0.5, b, 1.5, x)
 * Only valid for x<0, accurate at 0.1% level.
double hyp2f1trunc(double b, double x)
{
    double result;
    if (x > -1e4)
        return pow(1-x, -0.5)*std::tr1::hyperg(0.5, 1.5-b, 1.5, x/(x-1));
    else if (b > 0.4999 and b < 0.5001)
        return asinh(pow(-x, 0.5))/pow(-x, 0.5);
    else if (b > 0)
        return pow(-x, -b)/(1-2*b) + pow(M_PI, 0.5)*tgamma(b-0.5)*pow(-x, -0.5)/2/tgamma(b);
    else  // b < 0
        return pow(-x, -b)/(1-2*b);
}
*/

/* Returns directly results for the los integral 
 * int_s0^s1 ds s0^g0/sqrt(d^2 + s^2)^g0.
 */

double int_invsqrt(double g0, double d, double s0, double s1)
{
    double x0 = pow(s0/d, 2);
    double x1 = pow(s1/d, 2);
    double b = -g0/2;
    if (b > 0.4999 and b < 0.5001)
        return d*(asinh(s1/d) - asinh(s0/d));
    else if (x0 < 1e4)
        return s1*pow(1+x1, -0.5)*gsl_sf_hyperg_2F1(0.5, 1.5-b, 1.5, x1/(x1+1)) 
              -s0*pow(1+x0, -0.5)*gsl_sf_hyperg_2F1(0.5, 1.5-b, 1.5, x0/(x0+1));
    else
        return s1*pow(x1, -b)/(1-2*b)-s0*pow(x0, -b)/(1-2*b);
}

/* Calculates line of sight integrals, tabulated according to profile grid,
 * assuming viewer is outside of the dark matter halo.  Note that f and r need
 * to be non-zero everywhere.
 */
void los_integral_cpp(
        std::vector<double> r, std::vector<double> f, double D,  // Input
        std::vector<double> & phi, std::vector<double> & los // Output
        )
{
    // Check input consistency.
    if (r.size() != f.size())
        throw std::length_error("Incompatible vector lengths.");
    int Nin = r.size();

    // Are we inside the halo?
    bool inside = r.end()[-1] > D;

    // Set up profiles indices.
    std::vector<double> gamma(Nin);
    for (int i = 0; i<Nin-1; i++)
    {
        gamma[i] = log(f[i+1]/f[i])/log(r[i+1]/r[i]);
        //std::cout << gamma[i] << std::endl;
    //    if ( fabs(gamma[i]) > 5 )
    //       std::cout << "WARNING: Steep radial index might cause problems." << std::endl;
    }

    // Set up angles.
    phi.resize(0);
    for (int i = 0; r[i] < D and i < Nin; i++)
    {
        phi.push_back(asin(r[i]/D)*180/M_PI);
    }
    if (inside)
    {
        while (phi.back() < 180)
        {
            phi.push_back(phi.back() +10);
        }
        phi.back() = 180;
    }
    los.resize(phi.size());

    // Construct result.
    for (size_t i = 0; i<phi.size(); i++)  // Loop over phi
    {
        los[i] = 0;
        for (int j = inside ? 0 : i; j<Nin-1; j++)  // Loop over rings
        {
            double d = sin(phi[i]*M_PI/180)*D;  // Impact parameter of line of sight.
            double f0 = f[j];
            double r0 = r[j];
            double r1 = r[j+1];
            if ( r1 <= d ) continue;  // Inner rings don't matter.
            double g0 = gamma[j];
            double s0 = sqrt(std::max(pow(r0, 2) - pow(d, 2), 0.));  // >=0
            double s1 = sqrt(pow(r1, 2) - pow(d, 2));  // >0

            if (not inside)  // Standard integral.
            {
                los[i] += 2*f0*pow(d/r0, g0)*int_invsqrt(g0, d, s0, s1);
            }
            else
            {
                double sobs = sqrt(pow(D, 2) - pow(d, 2));
                if (phi[i] <= 90)
                {
                    los[i] += f0*pow(d/r0, g0)*int_invsqrt(g0, d, s0, s1);  // Further end.
                    if (sobs > s0)
                        los[i] += f0*pow(d/r0, g0)*int_invsqrt(g0, d, s0, std::min(s1, sobs));
                }
                else
                {
                    if (s1 > sobs)
                        los[i] += f0*pow(d/r0, g0)*int_invsqrt(g0, d, std::max(s0,sobs), s1);
                }
            }
        }
    }
}
