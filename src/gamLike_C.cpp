////////////////////////////////////////////////////////////
// GamLike C-API definitions
//
// Author: Christoph Weniger <c.weniger@uva.nl>
// Created: 2015-09-16
////////////////////////////////////////////////////////////

#include "gamLike.hpp"
#include "gamLike_C.h"
#include <gsl/gsl_integration.h>

#include <iostream>

namespace gamLike
{
    // Interface functions
    extern "C" void set_data_path_(char const* const mypath)
    {
        set_data_path((std::string)mypath);
    }

    extern "C" void init_(Experiments &tag)
    {
        init(tag);
    }

    extern "C" double lnl_(Experiments &tag,
            double const* const E, double const* const dPhidE, int &size)
    {
        std::vector<double> x ( E, E + size );
        std::vector<double> y ( dPhidE, dPhidE + size );
        return lnL(tag, x, y);
    }

    extern "C" void set_halo_profile_(Halos &tag,
            double const* const r, double const* const rho,
            int &size, double & dist)
    {
        std::vector<double> x ( r, r + size );
        std::vector<double> y ( rho, rho + size );
        set_halo_profile(tag, x, y, dist);
    }
}
