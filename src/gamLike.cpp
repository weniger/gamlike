////////////////////////////////////////////////////////////
// GamLike C++ API definitions
//
// Author: Christoph Weniger <c.weniger@uva.nl>
// Created: 2015-09-16
////////////////////////////////////////////////////////////


#include "gamLike.hpp"
#include "gamLike/experiments.hpp"
#include "gamLike/classes.hpp"
#include "gamLike/los_integral.hpp"
#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <sstream>
#include <math.h>
#include <gsl/gsl_integration.h>

using std::string;
using std::vector;
using std::pair;
using std::cout;
using std::endl;

namespace gamLike
{
    std::vector<Experiments> used_Experiments;
    std::map<Halos,
        std::pair<std::vector<double>,std::vector<double> > > profiles;
    std::map<Halos,
        std::pair<std::vector<double>,std::vector<double> > > emission;

    // Interface functions
    extern "C" void set_data_path(const std::string & mypath)
    {
        set_experiments_path(mypath);
    }

    extern "C" void init(Experiments tag)
    {
        switch(tag) {
            case FermiLAT_dSphs_2013:
                init_fermi_dwarfs_pass7();
                break;
            case FermiLAT_dSphs_2015:
                init_fermi_dwarfs_pass8();
                break;
            case FermiLAT_GC_CaloreA_2014:
                init_fermi_gc_CaloreA();
                break;
            case FermiLAT_GC_CaloreB_2014:
                init_fermi_gc_CaloreB();
                break;
            case FermiLAT_GC_Achterberg_2015:
                init_fermi_gc_Calore_HEP();
                break;
            case CTA_Silverwood:
                init_CTA_Silverwood();
                break;
            case HESS_Abramowski_integrated:
                init_HESS_Abramowski_integrated();
                break;
            case HESS_Abramowski_binned:
                init_HESS_Abramowski_binned();
                break;
            case FermiLAT_GC_Calore_2014_ExtJ:
                init_fermi_gc_Calore_ExtJ();
                break;
            case HESS_Abramowski_integrated_ExtJ:
                init_HESS_Abramowski_integrated_ExtJ();
                break;
            case HESS_Abramowski_binned_ExtJ:
                init_HESS_Abramowski_binned_ExtJ();
                break;
            default:
                std::cout << 
                    "gamLike ERROR: "
                    "Trying to initialize non-existing experiment."
                    << std::endl;
                exit(1);
        }
        used_Experiments.push_back(tag);
    }

    extern "C" double lnL(Experiments tag,
            const std::vector<double> & E,
            const std::vector<double> & dPhidE_original)
    {
        init(tag);
        std::vector<double> dPhidE = trunc_vec(dPhidE_original, 1e100);
        switch(tag) {
            case FermiLAT_dSphs_2013:
                return lnL_fermi_dwarfs_pass7(E, dPhidE);
            case FermiLAT_dSphs_2015:
                return lnL_fermi_dwarfs_pass8(E, dPhidE);
            case FermiLAT_GC_CaloreA_2014:
                return lnL_fermi_gc_CaloreA(E, dPhidE);
            case FermiLAT_GC_CaloreB_2014:
                return lnL_fermi_gc_CaloreB(E, dPhidE);
            case FermiLAT_GC_Achterberg_2015:
                return lnL_fermi_gc_Calore_HEP(E, dPhidE);
            case CTA_Silverwood:
                return lnL_CTA_Silverwood(E, dPhidE);
            case HESS_Abramowski_integrated:
                return lnL_HESS_Abramowski_integrated(E, dPhidE);
            case HESS_Abramowski_binned:
                return lnL_HESS_Abramowski_binned(E, dPhidE);
            case FermiLAT_GC_Calore_2014_ExtJ:
                return lnL_fermi_gc_Calore_ExtJ(E, dPhidE);
            case HESS_Abramowski_integrated_ExtJ:
                return lnL_HESS_Abramowski_integrated_ExtJ(E, dPhidE);
            case HESS_Abramowski_binned_ExtJ:
                return lnL_HESS_Abramowski_binned_ExtJ(E, dPhidE);
            default:
                std::cout << 
                    "gamLike ERROR: "
                    "Trying to retrieve lnL for non-existing experiment."
                    << std::endl;
                exit(1);
        }
        return 0;
    }

    extern "C" std::map<Experiments, double> 
        lnL_map(const std::vector<double> & E,
                const std::vector<double> & dPhidE)
    {
        std::map<Experiments, double> outmap;
        for(std::vector<Experiments>::iterator tag=used_Experiments.begin();
                tag!=used_Experiments.end(); tag++)
        {
            outmap[*tag] = lnL(*tag, E, dPhidE);
        }
        return outmap;
    }

    extern "C" void set_halo_profile(Halos tag,
            const std::vector<double> & r,
            const std::vector<double> & rho, double dist)
    {
        profiles[tag] = 
            std::pair<std::vector<double>,std::vector<double> > (r, rho);
        std::vector<double> phi;
        std::vector<double> rho2 = rho;
        std::vector<double> intensity;
        for ( size_t i = 0; i < rho2.size(); i++)
            rho2[i] = rho2[i] * rho2[i];
        los_integral_cpp(r, rho2, dist, phi, intensity);
        for ( size_t i = 0; i < intensity.size(); i++)
            intensity[i] *= 3.0856775814913684e21;  // kpc/cm
        emission[tag] =
            std::pair< std::vector<double>, std::vector<double> > 
            (phi, intensity);
    }
}
