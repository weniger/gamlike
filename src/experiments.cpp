////////////////////////////////////////////////////////////
// GamLike experiment definitions
//
// Author: Christoph Weniger <c.weniger@uva.nl>
// Created: 2015-09-16
////////////////////////////////////////////////////////////

#include "gamLike/classes.hpp"
#include "gamLike/experiments.hpp"
#include "gamLike.hpp"
#include <gsl/gsl_integration.h>

namespace gamLike
{
    // Global pointers and variables
#ifdef GAMLIKE_DATA_PATH
    std::string path = GAMLIKE_DATA_PATH;
#else
    std::string path;
#endif

    void set_experiments_path(const std::string & mypath)
    {
        path = mypath;
    }


    // Pass7 Likelihoods
    GamCombLike *dwarfsCombLike7 = NULL;

    void init_fermi_dwarfs_pass7()
    {
        if (dwarfsCombLike7 != NULL) return;
        dwarfsCombLike7 = new GamCombLike();
        dwarfsCombLike7->add_target(path+"Ackermann+_2013/like_bootes_I.txt", 18.8, 0.22, PROFILE_DIV_J, 0);
        //dwarfsCombLike7->add_target(path+"Ackermann+_2013/like_canes_venatici_I.txt", 17.7, 0.26, PROFILE_DIV_J, 0);
        dwarfsCombLike7->add_target(path+"Ackermann+_2013/like_canes_venatici_II.txt", 17.9, 0.25, PROFILE_DIV_J, 0);
        dwarfsCombLike7->add_target(path+"Ackermann+_2013/like_carina.txt", 18.1, 0.23, PROFILE_DIV_J, 0);
        dwarfsCombLike7->add_target(path+"Ackermann+_2013/like_coma_berenices.txt", 19.0, 0.25, PROFILE_DIV_J, 0);
        dwarfsCombLike7->add_target(path+"Ackermann+_2013/like_draco.txt", 18.8, 0.16, PROFILE_DIV_J, 0);
        dwarfsCombLike7->add_target(path+"Ackermann+_2013/like_fornax.txt", 18.2, 0.21, PROFILE_DIV_J, 0);
        dwarfsCombLike7->add_target(path+"Ackermann+_2013/like_hercules.txt", 18.1, 0.25, PROFILE_DIV_J, 0);
        //dwarfsCombLike7->add_target(path+"Ackermann+_2013/like_leo_I.txt", 17.7, 0.18, PROFILE_DIV_J, 0);
        dwarfsCombLike7->add_target(path+"Ackermann+_2013/like_leo_II.txt", 17.6, 0.18, PROFILE_DIV_J, 0);
        dwarfsCombLike7->add_target(path+"Ackermann+_2013/like_leo_IV.txt", 17.9, 0.28, PROFILE_DIV_J, 0);
        dwarfsCombLike7->add_target(path+"Ackermann+_2013/like_sculptor.txt", 18.6, 0.18, PROFILE_DIV_J, 0);
        dwarfsCombLike7->add_target(path+"Ackermann+_2013/like_segue_1.txt", 19.5, 0.29, PROFILE_DIV_J, 0);
        dwarfsCombLike7->add_target(path+"Ackermann+_2013/like_sextans.txt", 18.4, 0.27, PROFILE_DIV_J, 0);
        //dwarfsCombLike7->add_target(path+"Ackermann+_2013/like_ursa_major_I.txt", 18.3, 0.24, PROFILE_DIV_J, 0);
        dwarfsCombLike7->add_target(path+"Ackermann+_2013/like_ursa_major_II.txt", 19.3, 0.28, PROFILE_DIV_J, 0);
        dwarfsCombLike7->add_target(path+"Ackermann+_2013/like_ursa_minor.txt", 18.8, 0.19, PROFILE_DIV_J, 0);
        dwarfsCombLike7->add_target(path+"Ackermann+_2013/like_willman_1.txt", 19.1, 0.31, PROFILE_DIV_J, 0);
        // Only 15 of the dwarfs were used for the Fermi results
    }

    double lnL_fermi_dwarfs_pass7(const std::vector<double> & E,
            const std::vector<double> & dPhidE)
    {
        return dwarfsCombLike7->lnL(E, dPhidE);
    }


    // Pass8 Likelihoods
    GamCombLike *dwarfsCombLike8 = NULL;

    void init_fermi_dwarfs_pass8()
    {
        if (dwarfsCombLike8 != NULL) return;
        dwarfsCombLike8 = new GamCombLike();
        //dwarfsCombLike8->add_target(path+"Ackermann+_2015/like_bootes_III.txt", 0, 0, PROFILE, 0);
        //dwarfsCombLike8->add_target(path+"Ackermann+_2015/like_bootes_II.txt", 0, 0, PROFILE, 0);
        dwarfsCombLike8->add_target(path+"Ackermann+_2015/like_bootes_I.txt", 18.8, 0.22, PROFILE, 0);
        dwarfsCombLike8->add_target(path+"Ackermann+_2015/like_canes_venatici_II.txt", 17.9, 0.25, PROFILE, 0);
        //dwarfsCombLike8->add_target(path+"Ackermann+_2015/like_canes_venatici_I.txt", 17.7, 0.26, PROFILE, 0);
        //dwarfsCombLike8->add_target(path+"Ackermann+_2015/like_canis_major.txt", 0, 0, PROFILE, 0);
        dwarfsCombLike8->add_target(path+"Ackermann+_2015/like_carina.txt", 18.1, 0.23, PROFILE, 0);
        dwarfsCombLike8->add_target(path+"Ackermann+_2015/like_coma_berenices.txt", 19.0, 0.25, PROFILE, 0);
        dwarfsCombLike8->add_target(path+"Ackermann+_2015/like_draco.txt", 18.8, 0.16, PROFILE, 0);
        dwarfsCombLike8->add_target(path+"Ackermann+_2015/like_fornax.txt", 18.2, 0.21, PROFILE, 0);
        dwarfsCombLike8->add_target(path+"Ackermann+_2015/like_hercules.txt", 18.1, 0.25, PROFILE, 0);
        dwarfsCombLike8->add_target(path+"Ackermann+_2015/like_leo_II.txt", 17.6, 0.18, PROFILE, 0);
        //dwarfsCombLike8->add_target(path+"Ackermann+_2015/like_leo_I.txt", 17.7, 0.18, PROFILE, 0);
        dwarfsCombLike8->add_target(path+"Ackermann+_2015/like_leo_IV.txt", 17.9, 0.28, PROFILE, 0);
        //dwarfsCombLike8->add_target(path+"Ackermann+_2015/like_leo_V.txt", 0, 0, PROFILE, 0);
        //dwarfsCombLike8->add_target(path+"Ackermann+_2015/like_pisces_II.txt", 0, 0, PROFILE, 0);
        //dwarfsCombLike8->add_target(path+"Ackermann+_2015/like_sagittarius.txt", 0, 0, PROFILE, 0);
        dwarfsCombLike8->add_target(path+"Ackermann+_2015/like_sculptor.txt", 18.6, 0.18, PROFILE, 0);
        dwarfsCombLike8->add_target(path+"Ackermann+_2015/like_segue_1.txt", 19.5, 0.29, PROFILE, 0);
        //dwarfsCombLike8->add_target(path+"Ackermann+_2015/like_segue_2.txt", 0, 0, PROFILE, 0);
        dwarfsCombLike8->add_target(path+"Ackermann+_2015/like_sextans.txt", 18.4, 0.27, PROFILE, 0);
        dwarfsCombLike8->add_target(path+"Ackermann+_2015/like_ursa_major_II.txt", 19.3, 0.28, PROFILE, 0);
        //dwarfsCombLike8->add_target(path+"Ackermann+_2015/like_ursa_major_I.txt", 18.3, 0.24, PROFILE, 0);
        dwarfsCombLike8->add_target(path+"Ackermann+_2015/like_ursa_minor.txt", 18.8, 0.19, PROFILE, 0);
        dwarfsCombLike8->add_target(path+"Ackermann+_2015/like_willman_1.txt", 19.1, 0.31, PROFILE, 0);
        // Only 15 of the dwarfs were used for the Fermi results
    }

    double lnL_fermi_dwarfs_pass8(const std::vector<double> & E,
            const std::vector<double> & dPhidE)
    {
        return dwarfsCombLike8->lnL(E, dPhidE);
    }

    // GC likelihoods
    GamCombLike *gcCombLikeCaloreJFix = NULL;

    void init_fermi_gc_CaloreA()
    {
        if (gcCombLikeCaloreJFix != NULL) return;
        gcCombLikeCaloreJFix = new GamCombLike();
        // Note: The "J-value" refers here and at similar places to J/Omega,
        // since tables contain average intensities
        GamLike d(path+"Calore+_2014/like_GC_Calore.txt", 23.315, 0.1, NONE, 1);
        gcCombLikeCaloreJFix->add_target(d);
    }

    double lnL_fermi_gc_CaloreA(const std::vector<double> & E,
            const std::vector<double> & dPhidE)
    {
        return gcCombLikeCaloreJFix->lnL(E, dPhidE);
    }

    // GC likelihoods
    GamCombLike *gcCombLikeCalore_ExtJ = NULL;

    void init_fermi_gc_Calore_ExtJ()
    {
        if (gcCombLikeCalore_ExtJ != NULL) return;
        gcCombLikeCalore_ExtJ = new GamCombLike();
        GamLike d(path+"Calore+_2014/like_GC_Calore.txt", 666, 666, EXTERNAL, 1);
        d.set_weighted_ROI(MilkyWay, path+"Calore+_2014/ROI.txt", 0.429);
        gcCombLikeCalore_ExtJ->add_target(d);
    }

    double lnL_fermi_gc_Calore_ExtJ(const std::vector<double> & E,
            const std::vector<double> & dPhidE)
    {
        return gcCombLikeCalore_ExtJ->lnL(E, dPhidE);
    }

    // GC likelihoods
    GamCombLike *gcCombLikeCalore = NULL;

    void init_fermi_gc_CaloreB()
    {
        if (gcCombLikeCalore != NULL) return;
        gcCombLikeCalore = new GamCombLike();
        gcCombLikeCalore->add_target(path+"Calore+_2014/like_GC_Calore.txt", 23.315 - 0.052/2.3023, 0.86/2.3023, MARGIN, 1);
    }

    double lnL_fermi_gc_CaloreB(const std::vector<double> & E,
            const std::vector<double> & dPhidE)
    {
        return gcCombLikeCalore->lnL(E, dPhidE);
    }

    // GC likelihoods
    GamCombLike *gcCombLikeCaloreHEP = NULL;

    void init_fermi_gc_Calore_HEP()
    {
        if (gcCombLikeCaloreHEP != NULL) return;
        gcCombLikeCaloreHEP = new GamCombLike();
        gcCombLikeCaloreHEP->add_target(path+"Calore+_2014/like_GC_Calore_withHEPunc.txt", 23.315 - 0.052/2.3023, 0.86/2.3023, MARGIN, 1);
    }

    double lnL_fermi_gc_Calore_HEP(const std::vector<double> & E,
            const std::vector<double> & dPhidE)
    {
        return gcCombLikeCaloreHEP->lnL(E, dPhidE);
    }

    // CTA likelihoods
    GamCombLike *CTASilverwood = NULL;

    void init_CTA_Silverwood()
    {
        if (CTASilverwood != NULL) return;
        CTASilverwood = new GamCombLike();
        // J-value corresponds to "ROI segment 1" (3<b<4 and l>1 corner; see
        // Silverwood+ 2014 for details about the adopted DM profile) 20.57 =
        // log10(3.7506e20)
        CTASilverwood->add_target(path+"Silverwood+_2014/100h_0.01sys.dat", 20.57, 0.1, NONE, 0);
    }

    double lnL_CTA_Silverwood(const std::vector<double> & E,
            const std::vector<double> & dPhidE)
    {
        return CTASilverwood->lnL(E, dPhidE);
    }


    // HESS-I GC likelihoods
    GamCombLike *HESSI_GC_binned= NULL;

    void init_HESS_Abramowski_binned()
    {
        if (HESSI_GC_binned != NULL) return;
        HESSI_GC_binned = new GamCombLike();
        // J-values are here average J-values (over the ROI) - fluxes in the
        // table are intensities (assuming NFW J-value with parameters as in
        // Abramowski+2011)
        HESSI_GC_binned->add_target(path+"Abramowski+_2011/HESS_Abramowski2011_binned.dat", 24.33, 0.1, NONE, 0);
    }

    double lnL_HESS_Abramowski_binned(const std::vector<double> & E,
            const std::vector<double> & dPhidE)
    {
        return HESSI_GC_binned->lnL(E, dPhidE);
    }

    GamCombLike *HESSI_GC_binned_ExtJ= NULL;

    void init_HESS_Abramowski_binned_ExtJ()
    {
        if (HESSI_GC_binned_ExtJ != NULL) return;
        HESSI_GC_binned_ExtJ = new GamCombLike();
        GamLike d(path+"Abramowski+_2011/HESS_Abramowski2011_binned.dat", 666, 666, EXTERNAL, 0);
        d.set_weighted_ROI(MilkyWay, path+"Abramowski+_2011/ROI_ON_minus_OFF.txt", 0.000598);
        HESSI_GC_binned_ExtJ->add_target(d);
    }

    double lnL_HESS_Abramowski_binned_ExtJ(const std::vector<double> & E,
            const std::vector<double> & dPhidE)
    {
        return HESSI_GC_binned_ExtJ->lnL(E, dPhidE);
    }

    GamCombLike *HESSI_GC_integrated = NULL;

    void init_HESS_Abramowski_integrated()
    {
        if (HESSI_GC_integrated != NULL) return;
        HESSI_GC_integrated = new GamCombLike();
        // J-values are here average J-values (over the ROI) - fluxes in the
        // table are intensities (assuming NFW J-value with parameters as in
        // Abramowski+2011)
        HESSI_GC_integrated->add_target(path+"Abramowski+_2011/HESS_Abramowski2011_integrated.dat", 24.33, 0.1, NONE, 0);
    }

    double lnL_HESS_Abramowski_integrated(const std::vector<double> & E,
            const std::vector<double> & dPhidE)
    {
        return HESSI_GC_integrated->lnL(E, dPhidE);
    }

    GamCombLike *HESSI_GC_integrated_ExtJ = NULL;

    void init_HESS_Abramowski_integrated_ExtJ()
    {
        if (HESSI_GC_integrated_ExtJ != NULL) return;
        HESSI_GC_integrated_ExtJ = new GamCombLike();
        GamLike d(path+"Abramowski+_2011/HESS_Abramowski2011_integrated.dat", 666, 666, EXTERNAL, 0);
        d.set_weighted_ROI(MilkyWay, path+"Abramowski+_2011/ROI_ON_minus_OFF.txt", 0.000598);
        HESSI_GC_integrated_ExtJ->add_target(d);
    }

    double lnL_HESS_Abramowski_integrated_ExtJ(const std::vector<double> & E,
            const std::vector<double> & dPhidE)
    {
        return HESSI_GC_integrated_ExtJ->lnL(E, dPhidE);
    }
}
