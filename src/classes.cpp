////////////////////////////////////////////////////////////
// GamLike internal class definitions
//
// Author: Christoph Weniger <c.weniger@uva.nl>
// Created: 2015-09-16
////////////////////////////////////////////////////////////

#include "gamLike.hpp"
#include "gamLike/classes.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include <gsl/gsl_integration.h>

using std::string;
using std::vector;
using std::pair;
using std::cout;
using std::endl;

namespace gamLike
{
    // Truncate vector element wise
    std::vector<double> trunc_vec(const std::vector<double> & vec, double val)
    {
        std::vector<double> out;
        for (std::vector<double>::const_iterator it = vec.begin();
                it != vec.end(); it++)
        {
            out.push_back(std::max(std::min(val, *it), -val));
        }
        return out;
    }

    // Normal distribution with mean x0 and width dx.
    double normal_pdf(double x0, double dx, double x)
    {
        return exp(-pow(x-x0, 2)/2/pow(dx, 2))/sqrt(2*3.141592)/dx;
    }

    // Linear interpolation in lin-log space.
    double interpolate(double x, const vector<double> & xlist,
            const vector<double> & ylist, bool zerobound)
    {
        double x0, x1, y0, y1;
        int i = 1;
        if (zerobound)
        {
            if (x<xlist.front()) return 0;
            if (x>xlist.back()) return 0;
        }
        else
        {
            if (x<xlist.front()) return ylist.front();
            if (x>xlist.back()) return ylist.back();
        }
        // Find min i such that xlist[i]>=x.
        for (; xlist[i] < x; i++) {};
        x0 = xlist[i-1];
        x1 = xlist[i];
        y0 = ylist[i-1];
        y1 = ylist[i];
        // lin-vs-log interpolation for lnL vs flux
        return y0 + (y1-y0) * log(x/x0) / log(x1/x0);
    }

    // Initialize GamLike object.
    //
    // J_mean:
    //   log10(mean J-value [GeV^2/cm^5])
    //
    // J_err:
    //   log10(1sigma uncertainty of J-value), assuming log-normal distribution
    //
    // filename:
    //   data file
    //
    // filetype:
    //   0: lnL table
    //   1: full covariance matrix
    GamLike::GamLike(string filename, double J_mean, double J_err, jMode method,
            int filetype) :
        method(method), filetype(filetype), J_mean(J_mean), J_err(J_err),
        filename(filename)
    {
        if (filetype == 0)
            loadData_lnLtab(filename);
        if (filetype == 1)
            loadData_Sigma(filename);
    }

    double GamLike::lnLprod(double J)
    {
        double a1 = log(normal_pdf(this->J_mean, this->J_err, J));
        double a2 = this->lnL(this->my_Phi, J);
        return a1+a2;  // Return ln(likelihood)
    }

    // Internal static member function for gsl integration, takes void pointer
    // as class reference.
    double GamLike::invoke(double J, void* par)
    {
        GamLike* gamLike = static_cast<GamLike*>(par);
        return exp(gamLike->lnLprod(J));  // Return likelihood
    }

    // Likelihood evaluation.  Takes integrated, weighted spectra as argument.
    //
    // method = 0: no J-value uncertainty included
    // method = 1: marginalization
    // method = 2: profiling
    // method = 3: profiling like Ackermann:2013yva
    double GamLike::lnL(vector<double> Phi)
    {
        // GSL integration options.
        double epsabs = 0.;
        double epsrel = 0.001;
        size_t limit = 10000;
        double result, error;

        // Minimum lnL value of interest
        const double lnL_min = -1e5;

        // Skip marginalization/profiling if likelihood low or error small.
        if ( method == EXTERNAL )
        {
            double J_MW = getJvalue();
            return lnL(Phi, J_MW);
        }
        else if (J_err < J_mean*0.001 or (not (lnL(Phi, J_mean) > lnL_min))
                or method == NONE)
        {
            return lnL(Phi, this->J_mean);
        }
        else
        {
            function = &GamLike::invoke;
            params = this;
            my_Phi = Phi;

            // Marginalization over +-5 sigma range of J-values.
            if (method == MARGIN)
            {
                gsl_workspace = gsl_integration_workspace_alloc (100000);
                gsl_integration_qags(this, J_mean-5*J_err, J_mean+5*J_err,
                        epsabs, epsrel, limit, gsl_workspace, &result, &error);
                gsl_integration_workspace_free(gsl_workspace);

                return log(result);
            }
            // Profiling.  Maximize in the +-5 sigma J-value range, 1%
            // step-size.
            if (method == PROFILE)
            {
                double lnL_loc, lnL_max = lnL_min;
                for (double J = J_mean - 5*J_err; J < J_mean +5*J_err;
                        J = J + 0.05*J_err)  // Enough for 0.1% accuracy
                {
                    lnL_loc = lnLprod(J);
                    if (lnL_loc > lnL_max) lnL_max = lnL_loc;
                }
                return lnL_max;
            }
            // Profiling in the style of the Fermi paper:  Dividing by J is
            // what is done in Ackermann:2013yva. This introduce some hidden
            // prior dependence (namely on the parametrization of J), but
            // reproduces their results better.  This was also confirmed by
            // Email exchange with JC.
            if (method == PROFILE_DIV_J)
            {
                double lnL_loc, lnL_max = lnL_min;
                for (double J = J_mean - 5*J_err; J < J_mean +5*J_err;
                        J = J + 0.05*J_err)
                {
                    lnL_loc = lnLprod(J)-log(pow(10, J));
                    if (lnL_loc > lnL_max) lnL_max = lnL_loc;
                }
                return lnL_max;
            }
        }
        return -1e10;
    }

    // Getter function for energy bins underlying the likelihood function.
    E_bins GamLike::getEbins()
    {
        return e_bins;
    }

    void GamLike::set_weighted_ROI(Halos tag, const std::vector<double> & psi,
            const std::vector<double> & weight)
    {
        this->halo_tag = tag;
        this->weighted_ROI = std::pair<std::vector<double>,
            std::vector<double> >(psi, weight);
    }

    void GamLike::set_weighted_ROI(
            Halos tag, std::string filename, double Omega)
    {
        double x;
        std::vector<double> psi, weight;

        std::ifstream in(filename.c_str(), std::ios::binary);
        if (in.fail())
        {
            cout << "ERROR: Failed loading " + filename + "." << endl;
            exit(-1);
        }
        string line;
        while(std::getline(in, line))
        {
            if (line[0] == '#') continue;
            std::stringstream ss(line);

            // Read energy bins
            ss >> x;
            psi.push_back(x);
            ss >> x;
            weight.push_back(x/Omega);
        }
        this->set_weighted_ROI(tag, psi, weight);
    }

    double GamLike::getJvalue()
    {
        if ( emission[this->halo_tag].first.size() == 0 )
        {
            std::cout << "ERROR: Halo not set." << std::endl;
            exit(-1);
        }
        double J = 0;
        for ( size_t i = 0; i < weighted_ROI.first.size(); i++ )
        {
            J += interpolate(
                    weighted_ROI.first[i], emission[this->halo_tag].first,
                    emission[this->halo_tag].second, true)
                *weighted_ROI.second[i];
        }
        return log10(J);
    }

    // Load tabulated lnL data.  Adopts conventions for Fermi tabulated dwarf
    // likelihoods.
    //
    // Format:
    //   column 1: Emin [MeV]
    //   column 2: Emax [MeV]
    //   column 3: phi [MeV/cm2/s]  # Integrated flux times E
    //   column 4: lnL
    int GamLike::loadData_lnLtab(string filename)
    {
        double phi, lnL;
        int nlines = 0;
        pair<double, double> ebin, ebin_last;
        vector<double> phi_list, lnL_list;

        cout << "Load data for " + filename << "." << endl;
        std::ifstream in(filename.c_str(), std::ios::binary);
        if (in.fail())
        {
            cout << "ERROR: Failed loading " + filename + "." << endl;
            exit(-1);
        }
        string line;
        while(std::getline(in, line))
        {
            if (line[0] == '#') continue;
            std::stringstream ss(line);
            ss >> ebin.first;
            ebin.first /= 1e3;  // scale to GeV
            ss >> ebin.second;
            ebin.second /= 1e3;  // scale to GeV
            ss >> phi;
            phi /= 1e3;  // scale to GeV
            ss >> lnL;
            if ( nlines == 0 ) ebin_last = ebin;
            if ( ebin_last != ebin )
            {
                e_bins.push_back(ebin_last);
                ebin_last = ebin;
                lnL_vs_flux.push_back(pair<vector<double>, vector<double> >
                        (phi_list, lnL_list));
                phi_list.clear();
                lnL_list.clear();
            }
            phi += 1e-30;  // Avoid zero fluxes (because of log).
            phi_list.push_back(phi);
            lnL_list.push_back(lnL);
            nlines += 1;
        }
        e_bins.push_back(ebin);
        lnL_vs_flux.push_back(pair<vector<double>,
                vector<double> >(phi_list, lnL_list));
        in.close();
        return 0;
    }

    // Imports covariance matrix.
    //
    // Column 1: Emin [GeV]
    // Column 2: Emax [GeV]
    // Column 3-(2+n): Entries of n x n covariance matrix
    int GamLike::loadData_Sigma(string filename)
    {
        double tmp;
        pair<double, double> ebin;

        cout << "Load data for " + filename << "." << endl;
        std::ifstream in(filename.c_str(), std::ios::binary);
        if (in.fail())
        {
            cout << "ERROR: Failed loading " + filename + "." << endl;
            exit(-1);
        }
        string line;
        while(std::getline(in, line))
        {
            if (line[0] == '#') continue;
            std::stringstream ss(line);

            // Read energy bins
            ss >> ebin.first;
            ss >> ebin.second;
            e_bins.push_back(ebin);

            // Read flux
            ss >> tmp;
            this->flux.push_back(tmp);

            // Read Sigma
            vector<double> tmpVec;
            while(ss >> tmp)
            {
                tmpVec.push_back(tmp);
            }
            this->Sigma.push_back(tmpVec);
        }
        cout << "Flux vector length: " << this->flux.size() << endl;
        cout << "Sigma dim: " << this->Sigma.size()
            << " x " << this->Sigma.begin()->size() << endl;
        in.close();
        return 0;
    }

    // Retrieve lnL
    //
    //   J: J-value [GeV2/cm5]
    //   Phi: int(dNdE) * sv / m**2 / 8 pi [cm3/s/GeV2]
    double GamLike::lnL(vector<double> Phi, double J)
    {
        double lnL = 0;
        if (filetype == 0)
            lnL = lnL_lnLtab(Phi, J);
        if (filetype == 1)
            lnL = lnL_Sigma(Phi, J);
        return lnL;
    }

    // Retrieve lnL from covariance matrix (0.5 * chi2)
    double GamLike::lnL_Sigma(vector<double> Phi, double J)
    {
        double lnL = 0;
        int N = this->flux.size();
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
            {
                lnL -= 0.5*(Phi[i] * pow(10, J) - flux[i])
                    * (Phi[j] * pow(10, J) - flux[j]) * Sigma[i][j];
            }
        return lnL;
    }

    // Retrieve ln from likelihood tables
    double GamLike::lnL_lnLtab(vector<double> Phi, double J)
    {
        double lnL = 0, eMean, energy_flux;
        vector<double>::iterator it_Phi = Phi.begin();
        E_bins::iterator it_E_bin = e_bins.begin();
        for (LnL_vs_flux::iterator it = lnL_vs_flux.begin();
                it != lnL_vs_flux.end(); it++)
        {
            eMean = sqrt(it_E_bin->first * it_E_bin->second);  // [GeV]
            energy_flux = *it_Phi * pow(10, J) * eMean;  // GeV/cm^2/s
            lnL += interpolate(energy_flux, it->first, it->second, false);
            it_Phi++;
            it_E_bin++;
        }
        return lnL;
    }

    // Add target to the list of combined likelihoods
    //
    // Same parameters as GamLike constructor (except method, which is the same
    // for all).  Requires compatible energy bins.
    void GamCombLike::add_target(string filename,
            double J_mean, double J_err, jMode method, int filetype)
    {
        GamLike d(filename, J_mean, J_err, method, filetype);
        this->add_target(d);
    }

    void GamCombLike::add_target(GamLike & d)
    {
        if ( e_bins.empty() )
        {
            my_targets.push_back(d);
            e_bins = d.getEbins();
        }
        else
        {
            if ( e_bins == d.getEbins() )
            {
                my_targets.push_back(d);
            }
            else
            {
                cout << "gamLike ERROR: Incompatible energy bins." << endl;
                exit(-1);
            }
        }
    }

    // Returns lnL for combined likelihood, pre-binned.
    double GamCombLike::lnL(vector<double> Phi)
    {
        double lnL = 0;
        for (vector<GamLike>::iterator it =
                my_targets.begin(); it != my_targets.end(); it++)
        {
            lnL += it->lnL(Phi);
        }
        return lnL;
    }

    // Returns lnL for combined likelihood, but doing the energy integrals
    // internally.
    //
    // Takes as argument:
    //   E: Energy [GeV]
    //   Phi:  dNdE * sv / m**2 / 8 pi [cm3/s/GeV3]
    double GamCombLike::lnL(vector<double> E, vector<double> dPhidE)
    {
        vector<double> Phi;
        IntSpec intSpec(E, dPhidE);
        for (E_bins::iterator it = e_bins.begin(); it != e_bins.end(); it++)
        {
            double phi = intSpec.integrate(it->first, it->second);
            Phi.push_back(phi);
        }
        return lnL(Phi);
    }

    // Getter function for energy bins.
    E_bins GamCombLike::getEbins()
    {
        return e_bins;
    }

    // Constructor for integrated spectrum class, taking arrays of length 100
    // by default (used for coupling with SuperBayes).
    IntSpec::IntSpec(double* Energy, double* dNdE, int arraySize = 100)
    {
        function = &IntSpec::invoke;
        params=this;
        std::copy(&Energy[0], &Energy[arraySize], back_inserter(this->Energy));
        std::copy(&dNdE[0], &dNdE[arraySize], back_inserter(this->dNdE));
    }

    // Constructor for integrated spectrum class, taking arrays.
    IntSpec::IntSpec(vector<double> Energy, vector<double> dNdE)
    {
        function = &IntSpec::invoke;
        params=this;
        this->Energy = Energy;
        this->dNdE = dNdE;
    }

    /*
    // Do the integration.
    double IntSpec::integrate(double x0, double x1)
    {
        // For testing:
        //   return this->invoke(sqrt(x0*x1), this)*(x1-x0);
        double epsabs = 1e-30;
        double epsrel = 1e-3;
        size_t limit = 10000;
        double result, error;
        gsl_workspace = gsl_integration_workspace_alloc (100000);
        gsl_integration_qags(this,
           log(x0), log(x1), epsabs, epsrel,
           limit, gsl_workspace, &result, &error);
        gsl_integration_workspace_free(gsl_workspace);
        //std::cout << result << std::endl;
        return result;
    }
    */

    // Simple log-lin trapezoidal integration (robust)
    double IntSpec::integrate(double x0, double x1)
    {
        double y0, y1, s = 0;
        unsigned int i = 0, j = Energy.size()-1;
        x0 = std::max(Energy.front(), x0);
        x1 = std::min(Energy.back(), x1);
        if (x0 >= x1) return 0;

        y0 = interpolate(x0, Energy, dNdE, true);
        y1 = interpolate(x1, Energy, dNdE, true);
        // Find min i such that xlist[i]>x0
        for (; Energy[i] <= x0; i++) {};
        // Find max j such that xlist[j]<x1
        for (; Energy[j] >= x1; j--) {};

        s += (Energy[i]-x0)*(dNdE[i]+y0)/2;
        for (unsigned int k = i; k < j; k++)
        {
            s += (Energy[k+1]-Energy[k])*(dNdE[k]+dNdE[k+1])/2;
        }
        s += (x1-Energy[j])*(dNdE[j]+y1)/2;
        return s;
    }

    // GSL integrand.
    double IntSpec::invoke(double x, void* params)
    {
        IntSpec* p= static_cast<IntSpec*>(params);
        double t = interpolate(exp(x), p->Energy, p->dNdE, true);
        return t*exp(x);
    }
}
